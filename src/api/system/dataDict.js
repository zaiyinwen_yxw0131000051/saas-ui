import request from '@/utils/request'

export function loadDataUrl() {
  return 'admin-service/api/dataDictMng/loadData'
}

export function deleteUrl() {
  return 'admin-service/api/dataDictMng/doDelete'
}

export function toAdd(data) {
  return request({
    url: 'admin-service/api/dataDictMng/toAdd',
    method: 'post',
    data
  })
}

export function toCopy(data) {
  return request({
    url: 'admin-service/api/dataDictMng/toCopy',
    method: 'post',
    data
  })
}

export function viewMainData(data) {
  return request({
    url: 'admin-service/api/dataDictMng/toView',
    method: 'post',
    data
  })
}

export function addOrEdit(data) {
  return request({
    url: 'admin-service/api/dataDictMng/doSave',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: 'admin-service/api/dataDictMng/doDelete',
    method: 'post',
    data
  })
}

export function getDictByType(dictType) {
  const params = {
    dictType
  }
  return request({
    url: 'admin-service/api/dataDictMng/getDictByType',
    method: 'get',
    params
  })
}

